var https                   = require('https');
var http                    = require('http');
var express                 = require('express');
var path                    = require('path');
var bodyParser              = require('body-parser');
var fs                      = require('fs');

var users                   = require('./controllers/user.controller');
var complaint                = require('./controllers/complaint.controller')
var config                  = require('./config/config.json');
var mongojs                 = require('mongojs');
var moment                  = require('moment');
var port                    = 8088;
var app                     = express();


app.use(function (req, res, next) { //allow cross origin requests
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
    res.header("Access-Control-Allow-Origin", config.clientApiBaseUrl);
    var allowedOrigins = [config.clientApiBaseUrl];
    var origin         = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1){
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});



// Body Parser MW
app.use(bodyParser.json({ limit: "20mb" }));
app.use(bodyParser.urlencoded({ limit: "20mb", extended: true, parameterLimit: 20000 }));
app.use('/api/users', users);

app.use('/api/complaint', complaint);

var httpServer = http.createServer( app);
httpServer.setTimeout(300000);
httpServer.listen(port,function(){
    console.log('Server started on port',port)
});

﻿
var config                          = require('../config/config.json');


/**
 * Loading external libraries used
 */
var express                         = require('express');
var router                          = express.Router();
var http                            = require('http');
var Q                               = require('q');
var moment                          = require('moment');
var fs                              = require('fs');

var complaintService                = require('../services/complaint.service');

/**
 * List of all the route available in this module and respective function linked to it
 */

router.post('/LodgeNewComplaint', LodgeNewComplaint);
router.post('/fetchAllComplaints',fetchAllComplaints);
router.post('/getComplaint',getComplaint);
router.post('/addComments',addComments);
router.post('/updateStatus',updateStatus);
module.exports                          = router;

function LodgeNewComplaint(req, res) {

    console.log("api req",req.body);
    complaintService.LodgeNewComplaint(req.body)
        .then(function (response) {
            if (response) {
                res.json({ status: "success",complaint:response.complaint });
            } else {
                res.json({ status: "failure"});
            }
        })
        .catch(function (err) {
            res.json({ status: "failure", message: "Somthing Went Wrong"});
        });

}
function fetchAllComplaints(req, res) {

    // console.log("api req",req.body);
    complaintService.fetchAllComplaints(req.body)
        .then(function (response) {
            if (response) {
                res.json({ status: "success",complaintList:response.complaintList });
            } else {
                res.json({ status: "failure"});
            }
        })
        .catch(function (err) {
            res.json({ status: "failure", message: "Somthing Went Wrong"});
        });

}
function getComplaint(req, res){
        // console.log("api req",req.body);
        complaintService.getComplaint(req.body)
        .then(function (response) {
            if (response) {
                res.json({ status: "success",data:response.complaint });
            } else {
                res.json({ status: "failure"});
            }
        })
        .catch(function (err) {
            res.json({ status: "failure", message: "Somthing Went Wrong"});
        });
}

function addComments(req, res){
    
    complaintService.addComments(req.body)
    .then(function (response) {
        if (response) {
            res.json({ status: "success" });
        } else {
            res.json({ status: "failure"});
        }
    })
    .catch(function (err) {
        res.json({ status: "failure", message: "Somthing Went Wrong"});
    });
}
function updateStatus(req, res){
    
    complaintService.updateStatus(req.body)
    .then(function (response) {
        if (response) {
            res.json({ status: "success" });
        } else {
            res.json({ status: "failure"});
        }
    })
    .catch(function (err) {
        res.json({ status: "failure", message: "Somthing Went Wrong"});
    });
}


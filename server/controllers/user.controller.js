﻿var config = require('../config/config.json');
var express = require('express');
var router = express.Router();
var CryptoJS = require("crypto-js");
var userService = require('../services/user.service');
var base64 = require('base-64');

var Q = require('q');


// routes
router.post('/authenticate', authenticate);
router.post('/register', register);

router.post('/getUserInfo', getUserInfo);
router.post('/getRandomId', getRandomId)
module.exports = router;

function getRandomId(req, res) {
    userService.getRandomId(req.body)
        .then(function (response) {
           
            if (response) {
               
                res.json({ status: "success", message: "token loaded successfully", tokenId: response });
            } else {
                res.json({ status: "failed", message: "Failed to get token" });
            }
        })
        .catch(function (err) {
            res.json({ status: "failed", message: "Somthing Went Wrong" });
        });
}



function authenticate(req, res) {

    try{
        if (req.body.username && req.body.password && req.body.fingerprint && req.body.tokenId) {

            var decryptedUsername = base64.decode(req.body.username)
            var decryptedPassword = base64.decode(req.body.password)
            var decryptedTokenId = base64.decode(req.body.tokenId)

            // console.log("u : ",decryptedUsername," pass : ",decryptedPassword," token ",decryptedTokenId);
             decryptedTokenId = CryptoJS.AES.decrypt(decryptedTokenId, config.cipherKey).toString(CryptoJS.enc.Utf8);
             decryptedUsername = CryptoJS.AES.decrypt(decryptedUsername, decryptedTokenId);
             decryptedPassword = CryptoJS.AES.decrypt(decryptedPassword, decryptedTokenId);

            userService.authenticate(decryptedUsername.toString(CryptoJS.enc.Utf8), decryptedPassword.toString(CryptoJS.enc.Utf8), req.body.fingerprint, decryptedTokenId, req.body.params)
                .then(function (user) {
                    console.log("user", user)
                    if (user && user.login_status == 'success') {
                        // authentication successful
                        user.referenceId = req.body.fingerprint;
                        res.send({ status: "success", message: 'Login detail verified.', user: user });
    
                    } else if (user && user.login_status == 'failure') {
                        res.send({ status: "failure", message: 'Username or password is incorrect.', user: user });
                    } 
                    else {
                        // authentication failed
                        res.send({ status: "failure", message: 'Username or password is incorrect.' });
                    }
                })
                .catch(function (err) {
                    res.send({ status: "failure", message: 'Something went wrong.' });
                });
        } else {
            res.send({ status: "failure", message: 'Username or password is incorrect.' });
        }
    }catch (err) {
        res.send({ status: "failure", message: 'Something went wrong.' });
    }


    

}

function register(req, res) {

    console.log("body",req.body);
    req.body.password = CryptoJS.AES.decrypt(req.body.password, config.cipherKey).toString(CryptoJS.enc.Utf8);
    req.body.confirmPassword = CryptoJS.AES.decrypt(req.body.confirmPassword, config.cipherKey).toString(CryptoJS.enc.Utf8);
   console.log("res",req.body.confirmPassword);
    userService.create(req.body)
        .then(function (params) {

            if (params.status == "success") {
                // console.log(params);
                res.send({ status: "success", message: params.message, user: params.user });

            } else {
                // authentication failed
                res.send({ status: "failure", message: 'email already registered' });
            }
            //    console.log(prams);
            //    res.send(prams);

            res.sendStatus(200);
        })
        .catch(function (err) {
            res.send({ status: "failure", message: 'Something went wrong.' });
        });


}





function getUserInfo(req, res) {

    userService.getUserInfo(req.body)
        .then(function (userInfo) {
            if (userInfo) {
                res.json({ status: "success", message: "users fetched successfully", userInfo: userInfo });
            } else {
                res.json({ status: "failed", message: "Failed to list users info", userInfo: '' });
            }

        })
        .catch(function (err) {
            res.json({ status: "failed", message: "Somthing Went Wrong", userInfo: '' });
        });

}

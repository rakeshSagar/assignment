﻿/**
 * Loading Application level configuration data
 */
var config = require('../config/config.json');
var database = require('../config/database.js');

/**
 * Loading external libraries used
 */
var Q = require('q');
var mongojs = require('mongojs');
const path = require('path');
var moment = require('moment');

/**
 * List of all the functions available in this module
 */
var complaint = {};
complaint.LodgeNewComplaint = LodgeNewComplaint;
complaint.fetchAllComplaints = fetchAllComplaints;
complaint.getComplaint = getComplaint;
complaint.addComments = addComments;
complaint.updateStatus = updateStatus;
module.exports = complaint;

function LodgeNewComplaint(params) {
    var deferred = Q.defer();

    var complaintParams = {
        heading: params.heading,
        description: params.description,
        user_id: mongojs.ObjectId(params.loggedInUser.userId),
        status: "Request",
        date: moment().format(),
        last_update:moment().format()
    }

    
    database.complaints.insert(complaintParams, function (err, resp) {

        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve({
                status: "success",
                complaint: resp
            });
        }

    });

    return deferred.promise;
}

function fetchAllComplaints(params) {
    var deferred = Q.defer();

    console.log("user info", params.loggedInUser);
    database.users.findOne({
        _id: mongojs.ObjectId(params.loggedInUser)
    }, function (err, userObj) {

        console.log("userObj all complaints", userObj);
        if (userObj) {

            var query = {};

            if (userObj.user_type == "admin") {

            } else {
                query.user_id = mongojs.ObjectId(params.loggedInUser);
            }
            console.log("query",query)
            database.complaints.find(query, null, {
                sort: {
                    _id: -1
                }
            }, function (err, resp) {

                if (err) {
                    deferred.reject(err);
                } else {
                    // console.log("all complaints",resp);
                    deferred.resolve({
                        status: "success",
                        complaintList: resp
                    });
                }

            });

        } else {
            deferred.resolve({
                status: "failed",
                message: "User Not Found"
            });
        }

    });

    return deferred.promise;
}

function getComplaint(params) {

    var deferred = Q.defer();
    console.log("user info", params.loggedInUser);
    database.users.findOne({
        _id: mongojs.ObjectId(params.loggedInUser)
    }, function (err, userObj) {

        console.log("userObj", userObj);
        if (userObj) {

            database.complaints.findOne({
                _id: mongojs.ObjectId(params.complaintId)
            }, function (err, resp) {
                if (err) {
                    deferred.reject(err);
                } else {
                    resp.user_type = userObj.user_type;
                    console.log("all complaints", resp);
                    deferred.resolve({
                        status: "success",
                        complaint: resp
                    });
                }
            });

        }

    });
    return deferred.promise;

}


function addComments(params) {

    var deferred = Q.defer();
    database.users.findOne({
        _id: mongojs.ObjectId(params.loggedInUser.userId)
    }, function (err, userObj) {

        // console.log("userObj",userObj);
        if (userObj) {

            console.log("complaint req",params.compliantId);
            database.complaints.findOne({
                _id: mongojs.ObjectId(params.compliantId)
            }, function (err, complaint) {

                // var updateData = {};
                console.log("complaint",complaint);
                var commentData={
                    date_time : moment().format(),
                    description : params.comment,
                    user_type : userObj.user_type
                    }

                if ( complaint &&  complaint.comments) {

                   
                        complaint.comments.push(commentData);

                } else {
                    complaint.comments = [];
                     complaint.comments.push(commentData);

                   
                }

                
                // console.log("updateData -",complaint);
                database.complaints.update({
                    _id: mongojs.ObjectId(params.compliantId)
                }, {
                    $set: complaint
                }, function (err, resp) {
                    if (err) {
                        deferred.reject(err);
                    } else {
                        console.log("updated",resp);
                        deferred.resolve({
                            status: "success",
                            complaint: resp
                        });
                    }
                });

            });

        }

    });
    return deferred.promise;

}

function updateStatus(params) {

    var deferred = Q.defer();
    var StatusObj = {
        status:params.status,
        last_update:moment().format()
    }
            database.complaints.update({
                _id: mongojs.ObjectId(params.compliantId)
            },{ $set:StatusObj }, function (err, resp) {
                if (err) {
                    deferred.reject(err);
                } else {
                    deferred.resolve({
                        status: "success",
                        message:"updated succesfully"
                    });
                }
            });
    return deferred.promise;

}
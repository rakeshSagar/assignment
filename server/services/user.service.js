/**
 * Loading Application level configuration data
 */
var config = require('../config/config.json');
var database = require('../config/database.js');

/**
 * Loading external libraries used
 */

var Q = require('q');
var fs = require('fs');
var pbkdf2 = require('pbkdf2')
var crypto = require('crypto');

var moment = require('moment');
var mongojs = require('mongojs');
var http = require('http');
var CryptoJS = require("crypto-js");



/**
 * List of all the functions available in this module
 */

var user = {};
user.authenticate = authenticate;
user.create = create;

user.getUserInfo = getUserInfo;

user.getRandomId = getRandomId;
module.exports = user;


function genRandomString(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')/** convert to hexadecimal format */
        .slice(0, length);
    /** return required number of characters */
};




function authenticate(username, password, fingerprint, tokenId, params) {

    var deferred = Q.defer();
    try{
        database.login_tokens.findAndModify({
            query: { tokenId: tokenId,is_used:false }, update: {
                $set: {
                    is_used: true,
                }
            }
        }, function (err, token) {
    
            if (err) {
                console.log("error")
                deferred.reject({ status: "failure", message: 'Something went wrong!!' });
            }
            if (token) {
                database.users.findOne({ email_id: username}, function (err, user) {
                    console.log("---------user----------", user)
                    if (err) {
                        console.log("error")
                        deferred.reject({ status: "failure", message: 'Something went wrong!!' });
                    }
    
                    console.log("user========>", user)
    
                    if (user && !user.authentication.token) deferred.reject({ status: "success", message: 'Incomplete Profile' });
    
                    if (user && pbkdf2.pbkdf2Sync(password, user.authentication.token, 872791, 64, 'sha512').toString('hex') === user.authentication.hash) {
                        // authentication successful
                            user.login_status = 'success'
                            //deferred.resolve(user);
                            deferred.resolve(user);

    
                    } else {
                        if (user) {
                            user.login_status = 'failure'
                            deferred.resolve(user);
                        } else {
                            deferred.resolve();
                        }
    
                    }
                });
    
            }else{
                deferred.reject()
            }
        });
    }catch (err) {
        res.send({ status: "failure", message: 'Something went wrong.' });
    }

   
    return deferred.promise;
}


function getRandomId() {
    var deferred = Q.defer();

    var timeStamp = Math.floor(Date.now()) + genRandomString(2);
    
    var newToken = {
        tokenId: timeStamp,
        created_at: moment().format(),
        is_used: false
    }

    database.login_tokens.save(newToken, function (err, newToken) {
        if (err) {
            deferred.reject(err);
        }
        else {
            var encryptToken = CryptoJS.AES.encrypt(timeStamp, config.cipherKey).toString()
            deferred.resolve(encryptToken)
        }
    })
    return deferred.promise;

}


function create(params) {
    var deferred = Q.defer();
    // validation for username

    console.log("Regisre",params);
    database.users.findOne(
        { email_id: params.email },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (user) {
                deferred.resolve({ status: "failure", message: 'email already registered' });
            } else {
                var newParm = {};
                // newParm.userName =params.userName;
                newParm.email_id = params.email;
                var user = newParm;
                var salt = genRandomString(32)

                // adding salted hashed password to user object
                user.authentication = {};
                user.authentication.hash = pbkdf2.pbkdf2Sync(params.password, salt, 872791, 64, 'sha512').toString('hex');

                //storing salt key future reference
                user.authentication.token = salt;
                user.profile = {};
                user.profile.first_name = params.userName;
                user.profile.last_name = params.lastName;
                user.user_type = "customer";
                database.users.insert(user,
                    function (err, doc) {
                        if (err) deferred.reject(err.name + ': ' + err.message);
                            user.referenceId = params.fingerprint;
                            deferred.resolve({ status: "success", message: 'account created', user: user });

                        // deferred.resolve(user);
                    });


            }
        });

    return deferred.promise;


}

function getUserInfo(params) {

    var deferred = Q.defer();
    var userInfo = {};

    console.log("params.loggedInUser.userId",params.loggedInUser.userId);
    database.users.findOne({ _id: mongojs.ObjectId(params.loggedInUser.userId) }, function (err, users) {
        if (err) {
            // Report Error
           
            deferred.reject(err);
        }
        if (users) {

            userInfo.profileInfo = users.profile;
            userInfo.profileInfo.emailId = users.email_id;
            userInfo.profileInfo.type = users.user_type;
            userInfo.profileInfo.userId = users._id;
             console.log("user ifno",userInfo);
           deferred.resolve(userInfo);
        }   
    });
    return deferred.promise;
}


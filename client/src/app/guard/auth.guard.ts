﻿import { Injectable,OnDestroy } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppConfig } from '../app.config';
import { Data } from '../services/data.service';
import { Subscription } from "rxjs/Subscription"
import * as _ from 'underscore';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { AuthenticationService } from '../services/authentication.service';
import { Http, Headers, Response }           from '@angular/http';

@Injectable()
export class AuthGuard implements CanActivate {

  subscription: Subscription;
  public globalData: any;



  constructor(private http: Http,private router: Router,private data:Data,private authenticationService: AuthenticationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    if (localStorage.getItem('userToken') && sessionStorage.getItem('referenceId')) {

      var userToken = JSON.parse(localStorage.getItem('userToken'));
      console.log(sessionStorage.getItem('referenceId'),"cobal date",userToken);
     
      if (userToken.userId && userToken.referenceId && userToken.referenceId == sessionStorage.getItem('referenceId')) {
        console.log("matched");
        this.authenticationService.getUserInfo()
        .subscribe(response => {
          this.globalData        = response.userInfo;
          console.log("DAata",this.globalData);

        });
        return this.data.subscribeToDataService().map(e => {
          if (e){

            this.globalData = e;
            if(this.globalData){
              return true;
            }else{
              return false;
            }
          }else{
           
            
            this.redirect('/complaints');
          }
          }).catch(() => {
            var authRedirectUrl                 = '/login';
            this.router.navigate([authRedirectUrl])
            return Observable.of(false);
          }
        );
      }
    }else{
      var authRedirectUrl                 = '/login';

      // not logged in so redirect to login page with the return url
      this.router.navigate([authRedirectUrl], {queryParams: {returnUrl: state.url}});
      return Observable.of(false);
    }


  }
  

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  redirect(redirectUrl){
    this.authenticationService.getUserInfo()
    .subscribe(response => {
      this.globalData        = response.userInfo;
    
        this.data.onDataChange(this.globalData)
        this.router.navigate([redirectUrl])
      
    });
  }

}

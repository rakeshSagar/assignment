import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import * as Fingerprint2 from 'fingerprintjs2';
import { Data } from '../services/data.service';
import { AppConfig } from '../app.config';
import * as _ from 'underscore';
import * as crypto from 'crypto-js';


@Component({
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
  @ViewChild("captchaRef") captchaRefObj: any;
  static Username:string;
  public appConfig = new AppConfig(this.http);
  private loginForm: FormGroup;

  private loading: boolean = false;

  private returnUrl: string;
  private error: string = "";
  private fingerprint: string = "";
  public loginUserMode: string = "login";

  public tokenId: any
  public encryptedUserId:string;
  public passwordExpired:boolean=false;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    private http: Http,
    private data: Data

  ) {

    this.loginForm = fb.group({
      'username': [null, [Validators.required, Validators.email]],
      'userpassword': [null, Validators.required]
    });

  }


  ngOnInit() {
    // reset login status
    this.authenticationService.logout();
    var scope = this;
    new Fingerprint2().get(function (result, components) {
      scope.fingerprint = result //a hash, representing your device fingerprint
    });
  }


 


  login(post) {
    LoginComponent.Username = post.username;
     this.userService.getHash().subscribe(response => {
      if (response.status == 'success') {
        
        this.tokenId = response.tokenId;
        let decTokenId = crypto.AES.decrypt(this.tokenId, this.appConfig.cipherKey).toString(crypto.enc.Utf8);       
        this.loading = true;
        var emailId = crypto.AES.encrypt(post.username, decTokenId).toString();
        var password = crypto.AES.encrypt(post.userpassword, decTokenId).toString();
        emailId = btoa(emailId)
        //LoginComponent.Username = post.username;
        password = btoa(password)
        this.tokenId = btoa(this.tokenId)
        this.authenticationService.login(emailId, password, this.fingerprint, this.tokenId)
          .subscribe(data => {
            let response = data.json();
            if (response.status === "success") {
                    this.router.navigate(['/complaints']);
              localStorage.removeItem('config');
            } else {
              this.loading = false;
              this.error = response.message;
           }
          },
            error => {
              this.passwordExpired=true;
              this.error = error._body.message;
              this.loading = false;
            });
      }
      else {
        this.loading = false;
      }
    })
  }
}

import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent }       from './login.component';
// import { RegisterComponent }    from './register.component';




const routes: Routes = [
  {
    path: '',

    data: {
      title: 'Login Page'
    },
    children: [
      {
        path: '',
        component: LoginComponent,
        data: {
          title: 'Registration Page'
        }
      },
      // {
      //   path: 'registered',
      //   component: RegisterComponent,
      //   data: {
      //     title: 'Registration Page'
      //   }
      // }
    ]
  }
];




@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}

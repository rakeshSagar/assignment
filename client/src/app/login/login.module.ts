import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { LaddaModule } from 'angular2-ladda';

import { LoginComponent } from './login.component';
// import { RegisterComponent } from './register.component';
import { LoginRoutingModule } from './login-routing.module';


import { RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  imports: [ CommonModule,
  FormsModule,
  LoginRoutingModule,
  LaddaModule,
  ReactiveFormsModule,
  RecaptchaModule.forRoot()
  ],
  declarations: [
    LoginComponent,
    // RegisterComponent    

  ],
  providers: [
    AuthenticationService,
    UserService
    ]
})


export class LoginModule {

}






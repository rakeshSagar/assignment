import { Component } from '@angular/core';
import * as Fingerprint2        from 'fingerprintjs2';
import { Data } from './services/data.service';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  constructor(data : Data) {
    new Fingerprint2().get(function(result, components){
      sessionStorage.setItem('referenceId',result)
    });


  }

}

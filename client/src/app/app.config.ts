import { Http, Headers, Response }   from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable }                from '@angular/core';


// http://13.126.43.23:8088
@Injectable()
export class AppConfig{
  public serverAddress    : string  = "http://localhost:8088";
  public requestUID       : string  = '4eXgShgL7506Qcz/Ye031lj0c7KBnbzyYfCNqSKMIMk=';
  public cipherKey        : string  = '123456';
  

  public globalSetting    : Object  = {
    requestSignerMobile   : false
  }
  public loggedInUserInfo : any     = {
         userId  :'',
            key  :'',
     referenceId  :''
  };
  
  
  constructor(public http: Http) {
      if (localStorage.getItem('userToken')) {
        var userToken = JSON.parse(localStorage.getItem('userToken'));
        if(userToken.userId)      this.loggedInUserInfo.userId      = userToken.userId
        if(userToken.key)         this.loggedInUserInfo.token       = userToken.key
        if(userToken.referenceId) this.loggedInUserInfo.referenceId = userToken.referenceId
      }
  }
  
}


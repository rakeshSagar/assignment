﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';
import { User } from '../schema/user';

@Injectable()
export class UserService {
    private responseJson: any;
    public appConfig = new AppConfig(this.http);

    constructor(private http: Http) { }
    getHash(){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
       
        return this.http.post(this.appConfig.serverAddress + '/api/users/getRandomId', { headers: headers })
            .map(res => {

             this.responseJson= res.json()
            return this.responseJson;
            } );
    
        
    }
    
    createAuthorizationHeader(headers: Headers) {
        headers.append('Authorization', 'Basic ');
    }

    registration(postData) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(this.appConfig.serverAddress + '/api/users/register', JSON.stringify(postData), { headers: headers })
            .map((response: Response) => {

                this.responseJson = response.json();

                if (this.responseJson.status === "success") {
                    localStorage.setItem('userToken', JSON.stringify({
                        'userId': this.responseJson.user._id,
                        'key': this.responseJson.user.role,
                        'referenceId': this.responseJson.user.referenceId,
                    }));
                    return response;
                } else {
                    return response;
                }

            });

    }

    create(user: User) {
        return this.http.post(this.appConfig.serverAddress + '/api/users/register', user, this.jwt());
    }



    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }


}



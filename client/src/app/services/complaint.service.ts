﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';


@Injectable()
export class ComplaintService {
    private responseJson: any;
    public appConfig = new AppConfig(this.http);

    constructor(private http: Http) { }
    LodgeNewComplaint(params) {

        //logged user information
        params.loggedInUser = this.appConfig.loggedInUserInfo;
        
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // console.log(params)
        return this.http.post(this.appConfig.serverAddress + '/api/complaint/lodgeNewComplaint', JSON.stringify(params), { headers: headers })
            .map(res => res.json());
    }

    fetchAllComplaints(userid){

        console.log("calling user id",userid);
        var params = {
        loggedInUser : userid,
        
        }
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // console.log(params)
        return this.http.post(this.appConfig.serverAddress + '/api/complaint/fetchAllComplaints', JSON.stringify(params), { headers: headers })
            .map(res => res.json());

    }
    getComplaint(complaintId,userId){
        var params = {
            loggedInUser : userId,
            complaintId : complaintId
            }

            // console.log("user id -- ",userId);

            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            // console.log(params)
            return this.http.post(this.appConfig.serverAddress + '/api/complaint/getComplaint', JSON.stringify(params), { headers: headers })
                .map(res => res.json());
    }
    addComments(comment,compliantId){
        var params = {
            loggedInUser : this.appConfig.loggedInUserInfo,
            compliantId : compliantId,
            comment:comment
            }

            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            // console.log(params)
            return this.http.post(this.appConfig.serverAddress + '/api/complaint/addComments', JSON.stringify(params), { headers: headers })
                .map(res => res.json());
    }
    updateStatus(status,compliantId){
        var params = {
            loggedInUser : this.appConfig.loggedInUserInfo,
            status : status,
            compliantId:compliantId
            }

            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            // console.log(params)
            return this.http.post(this.appConfig.serverAddress + '/api/complaint/updateStatus', JSON.stringify(params), { headers: headers })
                .map(res => res.json());
    }



}



import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject"
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class Data {

  

  public test  = "";

  public storage: any;

  public allowedModules: any;

  public  storageSource = new BehaviorSubject<any>(null);
  storageObservable$ = this.storageSource.asObservable();
 
  
  onDataChange(data: any) {    
    this.storageSource.next(data);
  }


  subscribeToDataService(): Observable<Data[]> {
    console.log("observables");
    return this.storageSource.asObservable();
  }
  

  public constructor() {
  }

}

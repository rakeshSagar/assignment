﻿import { Injectable }                        from '@angular/core';
import { Http, Headers, Response }           from '@angular/http';
import { Observable }                        from 'rxjs/Observable';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';

import { AppConfig }                          from '../app.config';

@Injectable()
export class AuthenticationService {
   private responseJson :any;

   constructor(private http: Http, private config: AppConfig) { }

   login(username, password, fingerprint,tokenId){
   var params = {};
   
    return this.http.post(this.config.serverAddress + '/api/users/authenticate', {username: username, password: password,fingerprint: fingerprint,tokenId:tokenId,params:params})
      .map((response: Response) => {

          // login successful if there's a jwt token in the response
          this.responseJson = response.json();

          if (this.responseJson.status === "success") {

            console.log("response status",this.responseJson);
            // store username and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('userToken',JSON.stringify({
              'userId'      : this.responseJson.user._id,
              'key'         : this.responseJson.user.role,
              'referenceId' : this.responseJson.user.referenceId
            }));

            this.config.loggedInUserInfo.userId      = this.responseJson.user._id,
            this.config.loggedInUserInfo.token       = this.responseJson.user.role,
            this.config.loggedInUserInfo.referenceId = this.responseJson.user.referenceId

            // return true to indicate successful login
            return response;
          } else {
            // return false to indicate failed login
            return response;
          }

      })
  }

  logout() {
        // remove user from local storage to log user out
    localStorage.removeItem('userToken');
    // localStorage.clear();

  }


  getUserInfo(){
    var params = {
      loggedInUser  : this.config.loggedInUserInfo
    }
    var headers   = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.config.serverAddress + '/api/users/getUserInfo', JSON.stringify(params), { headers: headers })
        .map(res => res.json());
  }

}

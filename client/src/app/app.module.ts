import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule}     from '@angular/http';
import { AppConfig } from './app.config';

import { AppComponent } from './app.component';
import { AuthenticationService } from './services/authentication.service';


import { Data } from './services/data.service';

// Routing Module
import { AppRoutingModule } from './app.routing';
import { AuthGuard } from './guard/auth.guard';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';



//Directive

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpModule,
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
  ],
  providers: [AuthenticationService,{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },AppConfig,AuthGuard,Data],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

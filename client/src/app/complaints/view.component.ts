import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Http, Headers } from '@angular/http';
import { ComplaintService } from '../services/complaint.service';
import { AppConfig } from '../app.config';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  templateUrl: 'view.component.html'
})

export class ViewComponent implements OnInit {
  

  
  complaint:any= [];
   addComment: FormGroup;
  complaintId: string;
  noComments:boolean =false;
  showAddComment:boolean = false;
  statusUpdate:FormGroup;
  showUpdateForm:boolean = false;
  user_type:string;
  userId:string;
  updateStatus:boolean =false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private http: Http,
    private complaintService:ComplaintService,
    private authenticationService:AuthenticationService,

  ) {

    this.addComment = fb.group({
      'comments': [null, Validators.required]
    });
    this.statusUpdate = fb.group({
      'status': [null, [Validators.required]]
    });
  }

  backToview(){
    this.showAddComment =false;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      // console.log("params",params.complaintId);

      this.complaintId = params.complaintId;
      

  

    this.authenticationService.getUserInfo()
    .subscribe(response => {
    
      this.user_type = response.userInfo.profileInfo.type;
      this.userId= response.userInfo.profileInfo.userId;
      this.getComments(params.complaintId);

    });

    });
 
  } 

  updateCommentBtn(){
    this.showUpdateForm = true;
  }
  getComments(complaintId){
    this.complaintService.getComplaint(complaintId,this.userId)
      .subscribe(response => {
        console.log("response",response);
        if (response.status == "success") {
          this.complaint = response.data;
          if(response.data.comments){
          if(  response.data.comments.length > 0){
            
            this.noComments = false;
          }
          }else{
            this.noComments = true;
          }

          // console.log("res data",response.data);
          
        } else {
    
          // this.error         = response.message;
        }
      });
  }

  addComments(post){
    
    console.log(this.complaintId,"comments",post);
    this.complaintService.addComments(post.comments,this.complaintId)
    .subscribe(response => {
      // console.log("response",response);

      if(response.status=="success"){
        this.getComments(this.complaintId);
        this.showAddComment = false;

      }
    });
  }
  UpdateStatus(post){

   
    this.complaintService.updateStatus(post.status,this.complaintId)
    .subscribe(response => {
      // console.log("response",response);

      if(response.status=="success"){
        // this.getComments(this.complaintId);
        this.showUpdateForm = false;
        this.updateStatus =true;

      }
    });
    
  }
 
  addNewCommentBtn(){
    this.showAddComment = true;
  }

 
  
}

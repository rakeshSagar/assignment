import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Http, Headers } from '@angular/http';
import { UserService } from '../services/user.service';

import { ComplaintService } from '../services/complaint.service';
import { AuthenticationService } from '../services/authentication.service';


import { AppConfig } from '../app.config';

import * as _ from 'underscore';
import * as crypto from 'crypto-js';


@Component({
  templateUrl: 'complaints.component.html'
})

export class ComplaintsComponent implements OnInit {
  

  appConfig = new AppConfig(this.http);
  userId: string = this.appConfig.loggedInUserInfo.userId;
  closeResult: string;
  formOpen:boolean=false;
  complaintStatus:boolean =false;
  private lodgeComplaint: FormGroup;
  compliantsArray:any= [];
  noComplaints:boolean = false;
  user_type:string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private http: Http,
    private complaintService:ComplaintService,
    private authenticationService:AuthenticationService,

  ) {

    this.lodgeComplaint = fb.group({
      'heading': [null, [Validators.required]],
      'description': [null, Validators.required]
    });
  }

  backToComplaints(){
    this.formOpen=false;
  }
  ngOnInit() {

    this.authenticationService.getUserInfo()
    .subscribe(response => {
    
      this.user_type = response.userInfo.profileInfo.type;
      this.userId= response.userInfo.profileInfo.userId;
      
      this.fetchAllComplaints(this.userId);
    });

    
  } 
  LodgeNewComplaint(){
    this.formOpen=true;
  }
  LodgeComplaint(post){

    
    var params = {
      heading:post.heading,
      description:post.description
    }
    this.complaintService.LodgeNewComplaint(params)
    .subscribe(response => {
  // console.log("post");

  this.formOpen=false;
      if (response.status === "success") {
        this.complaintStatus = true;
        this.fetchAllComplaints(this.userId);
        
      } else {
        this.complaintStatus = false;
        // this.error         = response.message;
      }

    });
  }

  fetchAllComplaints(userId){
    this.complaintService.fetchAllComplaints(userId)
    .subscribe(response => {
      if (response.status === "success") {

        console.log(" sdflmsdf ",response.complaintList.length);
        if(response.complaintList.length > 0){
        this.compliantsArray = response.complaintList;
        this.noComplaints = false;
        }else{
          this.noComplaints = true;
        }
      } else {
  
        // this.error         = response.message;
      }
    });
  }


}

import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComplaintsComponent }       from './complaints.component';
import { ViewComponent }       from './view.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Complaints Page'
    },
    children: [
      {
        path: '',
        component: ComplaintsComponent,
        data: {
          title: 'Complaints Page'
        }
      },{
        path:'view/:complaintId',
        component:ViewComponent,
        data:{
          title:"Complaint View Page"
        }
      }
    ]
  }
];




@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintsRoutingModule {}


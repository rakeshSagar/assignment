import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { LaddaModule } from 'angular2-ladda';

import { ComplaintsComponent } from './complaints.component';
import { ViewComponent } from './view.component';

import { ComplaintsRoutingModule } from './complaints-routing.module';
import { ComplaintService } from '../services/complaint.service';

@NgModule({
  imports: [ CommonModule,
  FormsModule,
  LaddaModule,
  ReactiveFormsModule,
  ComplaintsRoutingModule
  ],
  declarations: [
    ComplaintsComponent,
    ViewComponent
  ],
  providers: [
    AuthenticationService,
    UserService,
    ComplaintService
  ]
})


export class ComplaintsModule {

}






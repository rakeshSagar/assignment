import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { AuthGuard } from './guard/auth.guard';

export const routes: Routes = [
  {
    path: 'login',
    component: SimpleLayoutComponent,
    data: {
      title: 'Login'
    },
    children: [
      {
        path: '',
        loadChildren: './login/login.module#LoginModule',
      }
    ]
  },
  {
    path: 'register',
    component: SimpleLayoutComponent,
    data: {
      title: 'Register'
    },
    children: [
      {
        path: '',
        loadChildren: './registration/registration.module#RegistrationModule',
      }
    ]
  },
  {
    path: 'complaints',
    component: FullLayoutComponent,
    data: {
      title: 'complaints'
    },
    canActivate:[AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './complaints/complaints.module#ComplaintsModule',
      }
    ]
  },
 
  

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { LaddaModule } from 'angular2-ladda';


import { RegisterComponent } from './register.component';
import { RegistrationRoutingModule } from './registration-routing.module';


@NgModule({
  imports: [ CommonModule,FormsModule,ReactiveFormsModule,RegistrationRoutingModule,
  LaddaModule
 ],
  declarations: [
    RegisterComponent,
  ],
  providers: [
    AuthenticationService,
    UserService
  ]
})
export class RegistrationModule {

constructor(){
 
}

}

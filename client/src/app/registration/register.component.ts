import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Http, Headers } from '@angular/http';

import { UserService } from '../services/user.service';

import { AuthenticationService } from '../services/authentication.service';

import * as Fingerprint2 from 'fingerprintjs2';

import * as crypto from 'crypto-js';

import { AppConfig } from '../app.config';



@Component({
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {


  public appConfig = new AppConfig(this.http);

  private error: string = "";

  private loading = false;
  
  
  resisterData: any;
  private loggedIn: boolean;
  private formActionCompleted: boolean = false;
  private registrationForm: FormGroup;
  private fingerprint: string = "";
  returnUrl: string;
  registered:boolean =false;
  

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService, 
      private http: Http
  ) {
    this.registrationForm = fb.group({
      'userName': [null, Validators.required],
      'lastName': [null, Validators.required],
      'email': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      'confirmPassword': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20), this.validatePasswordConfirmation.bind(this)]],
    });


  }



  validatePasswordConfirmation(control: FormControl): any {
    if (this.registrationForm) {
      return control.value === this.registrationForm.value.password ? null : { notSame: true }
    }
  }


  registration(post) {

    
    post.password         = crypto.AES.encrypt(post.password,this.appConfig.cipherKey).toString();
    post.confirmPassword  = crypto.AES.encrypt(post.confirmPassword,this.appConfig.cipherKey).toString();
    
    this.loading = true;
   
      this.resisterData = { "userName": post.userName, "email": post.email, "password": post.password, "confirmPassword": post.confirmPassword, "fingerprint": this.fingerprint, "lastName": post.lastName, "isActive": true };
      this.resisterData.userType = 'customer';
      this.userService.registration(this.resisterData)
        .subscribe(
        data => {
          this.loading = false;
          console.log(data);
          let response = data.json();
          console.log(response);
          if (response.status == "success") {
            
            this.registered =true;
            
          } else {
            
            this.error = response.message;
          }
         

        },
        error => {
          this.error = error._body.message;
          this.loading = false;
        });
    
  }


  ngOnInit() {
    var scope = this;
    new Fingerprint2().get(function (result, components) {
      scope.fingerprint = result //a hash, representing your device fingerprint
    });


  }
}
